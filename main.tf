resource "tls_private_key" "deployer_private_key" {
  algorithm = "RSA"
  rsa_bits = 4096
}
resource "local_file" "private_key" {
  content = tls_private_key.deployer_private_key.private_key_pem
  filename = "deployer_key.pem"
  file_permission = 0400
}
resource "aws_key_pair" "deployer_key" {
  key_name = "deployer-key"
  public_key = tls_private_key.deployer_private_key.public_key_openssh
}

