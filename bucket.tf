resource "aws_s3_bucket" "s3bucket" {
  bucket = var.bucket_name
  acl    = "public-read"

  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_object" "object" {
  bucket = aws_s3_bucket.s3bucket.id
  key    = "SampleApp.zip"
  #acl    = "private"  # or can be "public-read"
  source = "./SampleApp.tar.gz"
  etag = filemd5("./SampleApp.tar.gz")

}