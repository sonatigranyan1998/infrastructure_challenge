data "aws_ami" "amazon-linux-2" {
  most_recent = true
  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
  owners = ["amazon"]
}

##############instance

resource "aws_instance" "webserver" {
  ami                  = data.aws_ami.amazon-linux-2.id
  instance_type        = "t2.micro" 
  key_name             = aws_key_pair.deployer_key.key_name
  iam_instance_profile = aws_iam_instance_profile.main.name
  security_groups      = [aws_security_group.allow_http_ssh.name]

  tags = {
    Name = "CodeDeployDemo"
  }

  provisioner "remote-exec" {
    script = "./install_codedeploy_agent.sh"

    connection {
      agent       = false
      type        = "ssh"
      user        = "ec2-user"
      private_key = tls_private_key.deployer_private_key.private_key_pem
      host        = aws_instance.webserver.public_ip
      port        = 22
    }
  }
}


#########SG
resource "aws_security_group" "allow_http_ssh" {
  name        = "allow_http"
  description = "Allow http inbound traffic"
  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
 
  }
  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_http_ssh"
  }
}
