output bucket_name {
  value = "${aws_s3_bucket.s3bucket.bucket_domain_name}"
}

output instance_ip {
  value = "${aws_instance.webserver.public_ip}"
}
