provider "aws" {
  region = "us-east-2"
  profile = "sonata"
}

variable "region" {
  type    = string
  default = "us-east-2"
}

variable "bucket_name" {
  type    = string
  default = "code-deploy-demo-bucket-124351515145"
}