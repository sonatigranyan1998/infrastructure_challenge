This code assumes the following

AWS EC2 as the sample application system for the deployment

AWS EC2 Instance running Amazon Linux AMI

AWS S3 as the storage facility for deployment files

AWS CodeDeploy as the Deployment engine for the project

AWS CodePipeline as the Pipeline for deployment


## CodeDeploy with Terraform Demo

1. Set up AWS profile in variable.tf file.

2. Run terraform:

  $ terraform init
  $ terraform apply
  Resources will be created in eu-east-2 region. Change region var if you want another region. 


3. From the output get the name of the s3 bucket created and EC2 public IP for Access the Sample applicatio.
